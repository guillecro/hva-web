<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package _s
 */

?>


</div>
<div class="push"></div>
</div><!-- #page -->
</div><!-- #wrapper -->
<footer id="colophon" class="site-footer footer" role="contentinfo">
    <div class="container">
        <div class="row">
            <div class="col-sm-3 col-xs-12 hidden-md hidden-lg">
                <div class="img-container">
                    <img id="footer-logo" src="<?php echo get_template_directory_uri(); ?>/images/utils/logo.png" class="img-responsive" alt="">
                </div>
                <div class="text-center info-footer">
                    Saavedra 3252, Santa Fe, CP 3000
                    <br><i class="fa fa-phone"></i> +54 9 342 444-44444
                </div>
            </div>
            <div class="col-sm-2 col-xs-6">
                <ul class="primer-nivel">
                    <li><a href="<?php echo get_home_url() ?>">Inicio</a></li>
                    <li><a href="<?php echo get_permalink( get_page_by_path( 'familia' ) )?>">Familia Ganadera</a></li>
                    <li><a href="<?php echo get_permalink( get_page_by_path( 'empresa' ) )?>">La empresa</a></li>
                </ul>
            </div>
            <div class="col-sm-3 col-xs-6">
                <ul class="primer-nivel">
                    <li><a href="<?php echo get_permalink( get_page_by_path( 'ventas-particulares' ) )?>">Ventas</a></li>
                    <li>
                        <ul class="segundo-nivel">
                           <li><a href="<?php echo get_permalink( get_page_by_path( 'ventas-particulares' ) )?>">Particulares</a></li>
                            <?php wp_list_pages( array(
    'menu_class' => 'nav navbar-nav',
    'walker'     => new Bootstrap_Page_Menu(),
    'title_li'   => null,
    'child_of'   => 13,
    'depth'      => 2,
) )?>
                        </ul>
                    </li>
                </ul>
            </div>
            <div class="col-sm-2 col-xs-6">
                <ul class="primer-nivel">
                    <li><a href="<?php echo get_permalink( get_page_by_path( 'establecimientos' ) )?>">Establecimientos</a></li>
                </ul>
            </div>
            <div class="col-sm-2 col-xs-6">
                <ul class="primer-nivel">
                    <li><a href="<?php echo get_permalink( get_page_by_path( 'contacto' ) )?>">Contacto</a></li>
                    <li>
                        <ul class="segundo-nivel">
                            <li><a href="<?php echo get_permalink( get_page_by_path( 'contacto' ) )?>">Información General</a></li>
                            <li><a href="<?php echo get_permalink( get_page_by_path( 'reclamos' ) )?>">Reclamos</a></li>
                        </ul>
                    </li>
                </ul>
            </div>
            <div class="col-sm-3 col-xs-12 hidden-sm hidden-xs">
                <div class="img-container">
                    <img id="footer-logo" src="<?php echo get_template_directory_uri(); ?>/images/utils/logo.png" class="img-responsive" alt="">
                </div>
                <div class="text-center info-footer">
                    Saavedra 1650
                    <br>CP 3000 Santa Fe, Arg
                    <br><i class="fa fa-phone"></i> +54 9 4582248
                </div>
            </div>
        </div>
    </div>
</footer><!-- #colophon -->

<?php wp_footer(); ?>

</body>
</html>
