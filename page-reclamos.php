<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package _s
 */

get_header(); ?>

<!--           CONTENIDO A PARTIR DE ACA-->
<div id="contacto" class="container container-body info-block">
    <div class="row">
        <div class="col-xs-12">
            <div class="titulo-seccion text-center">
                <h1>R<span class="lowcase">ECLAMOS</span></h1>
            </div>
        </div>
    </div>
    <div class="row">


        <?php
        echo gcb('formulario-reclamos');
        ?>

    </div>
</div>
<?php get_footer(); ?>
