<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package _s
 */

get_header(); ?>

	<!--           CONTENIDO A PARTIR DE ACA-->
            <!--            <p>Your website content here.</p>-->
            <div id="familia" class="container container-body info-block">
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-8 col-md-offset-2">
                        <div class="titulo-seccion text-center">
                            <!--
<img src="../images/utils/deco-izq.png" class="deco" alt="">
<h1>L<span class="lowcase">A</span> E<span class="lowcase">MPRESA</span></h1>
<img src="../images/utils/deco-der.png" class="deco" alt="">
-->

                            <ul class="list-inline">
                                <li><img src="<?php echo get_template_directory_uri(); ?>/images/utils/deco-izq.png" class="deco" alt=""></li>
                                <li><h1>L<span class="lowcase">A</span> F<span class="lowcase">AMILIA</span> G<span class="lowcase">ANADERA</span></h1></li>
                                <li><img src="<?php echo get_template_directory_uri(); ?>/images/utils/deco-der.png" class="deco" alt=""></li>
                            </ul>

                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12 col-sm-6 col-md-4 col-md-offset-2">
                        <img src="<?php echo wp_get_attachment_image_src($attachment_id = 27, $size = 'full')[0]; ?>" alt="" class="img-responsive foto-familia">
                        <p class="epigrafe"><?php echo str_replace('</p>','',str_replace('<p>','',gcb('familia-foto1-epigrafe'))); ?></p>
                    </div>
                    <div class="col-xs-12 col-sm-6 col-md-4">
                        <p class="texto-seccion">

                           <?php
                            $parrafo = str_replace('</p>','',str_replace('<p>','',gcb('familia-parrafo1')));
                            $first_letter = substr($parrafo,0,1);
                            $rest = substr($parrafo,1);
                            ?>
                            <span class="firstcharacter"><?php echo $first_letter ?></span><?php echo $rest ?>
                        </p>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12 col-sm-6 col-md-4 col-md-offset-2">
                        <img src="<?php echo wp_get_attachment_image_src($attachment_id = 28, $size = 'full')[0]; ?>" alt="" class="img-responsive foto-familia">
                        <p class="epigrafe"><?php echo str_replace('</p>','',str_replace('<p>','',gcb('familia-foto2-epigrafe'))); ?></p>
                    </div>
                    <div class="col-xs-12 col-sm-6 col-md-4">
                        <p class="texto-seccion">
                            <?php
                            echo str_replace('</p>','',str_replace('<p>','',gcb('familia-parrafo2')));
                            ?>
                        </p>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-8 col-md-offset-2">
                        <img src="<?php echo wp_get_attachment_image_src($attachment_id = 31, $size = 'full')[0]; ?>" alt="" class="img-responsive foto-banner">
                    </div>
                </div>
            </div>
<?php
//get_sidebar();
?>
<?php get_footer(); ?>
