<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package _s
 */

get_header(); ?>

<!--           CONTENIDO A PARTIR DE ACA-->
<div id="ventas-particulares" class="container container-body info-block">
    <div class="row">
        <div class="col-xs-12 col-sm-10 col-sm-offset-1">
            <div class="titulo-seccion text-center">
                <h1>V<span class="lowcase">ENTAS PARTICULARES</span></h1>
            </div>
            <div class="texto-seccion">
                <p>
                    <?php
                    $content = apply_filters('the_content', $post->post_content);
                    echo $content;
                    ?>
                </p>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-8 col-sm-offset-2">
            <div class="col-md-6 contactos santa-fe">
                <h3 class=" posicion-contactos">En Santa Fe</h3>
                <?php
                echo gcb('ventas-particulares-santafe');
                ?>
            </div>
            <div class="col-md-6 contactos buenos-aires">
                <h3  class="posicion-contactos">En Buenos Aires</h3>
                <?php
                echo gcb('ventas-particulares-buenosaires');
                ?>
            </div>
        </div>
    </div>

</div>
<?php
//get_sidebar();
?>
<?php get_footer(); ?>
