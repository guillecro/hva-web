<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package _s
 */

get_header(); ?>

	<!--           CONTENIDO A PARTIR DE ACA-->
            <div id="fila1">
                <div class="container">
                    <div class="row">
                        <div class="col-md-5">
                            <div class="bloque">
                                <h3>N<span class="lowcase">OVEDADES</span></h3>
                                <div class="novedad-preview">
                                    <h4>24° Remate en Hersilia</h4>
                                    <p>Los bovinos de carne Hereford tuvieron su origen a comienzos del siglo XVIII en el condado de Herefordshire, Inglaterra, como consecuencia de trabajos realizados por Benjamín Tomkins, con ganado de su propiedad. Fue él uno de los primeros en dedicarse a mejorar animales exclusivamente para carne. Desde ese condado, pasó a los vecinos, luego a Irlanda, no tardando en extenderse por todo el mundo. Está definitivamente establecido que desde comienzos del siglo XVIII, ese ganado llamó la atención por poseer características distintivas de color, conformación, constitución, mansedumbre, tendencia carnicera y temperamento...

                                    </p>
                                    <a href="#">Leer mas <i class="fa fa-caret-down fa-fw"></i></a>
                                </div>
                                <div class="novedad-preview">
                                    <h4>Reproductores de pedrigree</h4>
                                    <p>Los bovinos de carne Hereford tuvieron su origen a comienzos del siglo XVIII en el condado de Herefordshire, Inglaterra, como consecuencia de trabajos realizados p...</p>
                                    <a href="#">Leer mas <i class="fa fa-caret-down fa-fw"></i></a>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-7">
                            <div class="bloque">
                                <h3>G<span class="lowcase">ALERIA</span></h3>
                                <div id="carousel-example-generic" class="carousel slide">
                                    <!-- Indicators -->
                                    <ol class="carousel-indicators hidden-xs">
                                        <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
                                        <li data-target="#carousel-example-generic" data-slide-to="1"></li>
                                        <li data-target="#carousel-example-generic" data-slide-to="2"></li>
                                    </ol>

                                    <!-- Wrapper for slides -->
                                    <div class="carousel-inner">
                                        <div class="item active">
                                            <img class="img-responsive img-full" src="<?php echo get_template_directory_uri(); ?>/images/fotos/slide-1.jpg" alt="">
                                        </div>
                                        <div class="item">
                                            <img class="img-responsive img-full" src="<?php echo get_template_directory_uri(); ?>/images/fotos/slide-2.jpg" alt="">
                                        </div>
                                        <div class="item">
                                            <img class="img-responsive img-full" src="<?php echo get_template_directory_uri(); ?>/images/fotos/slide-3.jpg" alt="">
                                        </div>
                                    </div>

                                    <!-- Controls -->
                                    <a class="left carousel-control" href="#carousel-example-generic" data-slide="prev">
                                        <span class="icon-prev"></span>
                                    </a>
                                    <a class="right carousel-control" href="#carousel-example-generic" data-slide="next">
                                        <span class="icon-next"></span>
                                    </a>
                                </div>
                            </div>


                        </div>
                    </div>
                </div>
            </div>
            <div id="fila2">
                <div class="container">
                    <div class="row">
                        <div class="col-xs-12 col-md-6 foto-familiar">
                            <img src="<?php echo get_template_directory_uri(); ?>/images/fotos/Flia.%20Ea.%20San%20Juan.jpg" class="" alt="">
                        </div>
                        <div class="col-xs-12 col-md-6">
                            <div class="bloque descripcion">
                                <h3>F<span class="lowcase">AMILIA DE GANADEROS</span></h3>
                                <p>Los bovinos de carne Hereford tuvieron su origen a comienzos del siglo XVIII en el condado de Herefordshire, Inglaterra, como consecuencia de trabajos realizados por Benjamín Tomkins, con ganado de su propiedad. Fue él uno de los primeros en dedicarse a mejorar animales exclusivamente para carne. Desde ese condado, pasó a los vecinos, luego a Irlanda, no tardando en extenderse por todo el mundo. Está definitivamente establecido que desde comienzos del siglo XVIII, ese ganado llamó la atención por poseer características distintivas de color, conformación, constitución, mansedumbre, tendencia carnicera y temperamento...</p>
                                <a href="#">Leer mas <i class="fa fa-caret-down fa-fw"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div id="fila3">
                <div class="container">
                    <div class="row">
                            <a href="#">
                        <div class="col-sm-12 col-md-5">
                                <div class="bloque">
                                    <h3><h3>E<span class="lowcase">SCUELA RURAL</span></h3></h3>
                                    <div class="bg"></div>
                                    <p>Los bovinos de carne Hereford tuvieron su origen a comienzos del siglo XVIII en el condado de Herefordshire, Inglaterra, como consecuencia de trabajos realizados por Benjamín Tomkins, con ganado de su propiedad.
                                    </p>
                                </div>
                        </div>
                            </a>
                        <a href="#">
                            <div class="col-sm-6 col-md-4">
                                <div class="bloque">
                                    <h3>C<span class="lowcase">HARLA SOBRE</span></h3>
                                    <div class="bg"></div>
                                    <p>Los bovinos de carne Hereford tuvieron su origen a comienzos del siglo XVIII en el condado de Herefordshire, Inglaterra, como consecuencia de trabajos realizados por Benjamín Tomkins, con ganado de su propiedad.</p>
                                </div>
                            </div>
                        </a>
                        <a href="#">
                            <div class="col-sm-6 col-md-3">
                                <div class="bloque">
                                    <h3>E<span class="lowcase">SCUELA N° 201</span></h3>
                                    <div class="bg"></div>
                                    <p>Los bovinos de carne Hereford tuvieron su origen a comienzos del siglo XVIII en el condado de Herefordshire, Inglaterra, como consecuencia de trabajos realizados por Benjamín Tomkins, con ganado de su propiedad.</p>
                                </div>
                            </div>
                        </a>
                    </div>
                </div>
            </div>
<?php
//get_sidebar();
?>
<?php get_footer(); ?>
