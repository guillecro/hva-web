<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package _s
 */

get_header(); ?>

<!--           CONTENIDO A PARTIR DE ACA-->
<div id="establecimientos" class="container container-body info-block">
    <div class="row">
        <div class="col-xs-12">
            <div class="titulo-seccion text-center">
                <h1>E<span class="lowcase">STABLECIMIENTOS</span></h1>
            </div>
             <div class="row">
        <div class="col-xs-12 col-sm-offset-1 col-sm-10 col-md-offset-2 col-md-8 papel">
         <div class="texto-seccion">
                <p>
                    <?php
                    $parrafo = str_replace('</p>','',str_replace('<p>','',apply_filters('the_content', $post->post_content)));
                            $first_letter = substr($parrafo,0,1);
                            $rest = substr($parrafo,1);
                    ?>
                    <span class="firstcharacter"><?php echo $first_letter ?></span><?php echo $rest ?></p>
            </div>
        </div>
    </div>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12 col-sm-offset-1 col-sm-10 col-md-offset-2 col-md-8 papel">
            <img src="<?php echo wp_get_attachment_image_src(get_post_thumbnail_id(), $size = 'full')[0]; ?>" alt="" class="img-responsive">
        </div>
    </div>
</div>
<?php
    //get_sidebar();
?>
<?php get_footer(); ?>
