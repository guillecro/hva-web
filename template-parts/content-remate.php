
<article id="post-<?php the_ID(); ?>" <?php post_class('remate'); ?>>
    <div class="row">
        <div class="col-md-7 col-md-offset-1">
            <header class="entry-header">

                <h1 class="titulo-remate text-center">
                    <span class="primera-parte">
                        <?php
                        $titulo = $post->post_title;
                        $pieces = explode(" ", $titulo);
                        $first_letter = substr($pieces[0],0,1);
                        $rest = substr($pieces[0],1);
                        ?>
                        <?php echo $first_letter ?><span class="lowcase"><?php echo $rest ?></span>
                    </span>
                    <br>
                    <span class="segunda-parte">
                        <?php
                        $titulo = str_replace($pieces[0] . ' ','',$post->post_title);
                        $first_letter = substr($titulo,0,1);
                        $rest = substr($titulo,1);
                        ?>
                        <?php echo $first_letter ?><span class="lowcase"><?php echo $rest ?></span>
                    </span>
                </h1>
                <!--                    <?php the_title( '<h1 class="titulo-remate text-center">', '</h1>' ); ?>-->
            </header><!-- .entry-header -->
        </div>
    </div>
    <div class="row">
        <div class="col-md-7 col-md-offset-1">
            <a href="<?php echo wp_get_attachment_url( get_post_thumbnail_id()); ?>">
                <img src="<?php echo wp_get_attachment_url( get_post_thumbnail_id()); ?>" class="img-responsive" alt="">
            </a>
            <div class="entry-content">
                <?php the_content(); ?>
                <h4 class="entry-title">Como llegar</h4>
                <a href="<?php echo get_post_meta($post->ID, 'pdf', true); ?>" class="btn btn-default"><i class="fa fa-file-o fa-fw"></i>Descargar PDF</a>
                <a href="https://www.google.com/maps/dir/Current+Location/<?php echo get_post_meta($post->ID, 'posicion', true); ?>" class="btn btn-default"><i class="fa fa-map-o fa-fw"></i> Ver en Google Maps</a>
            </div><!-- .entry-content -->
        </div>
        <div class="col-md-3">
            <div class="otros-remates">

                <ul class="">

                    <?php wp_list_pages( array(
    'menu_class' => 'list-inline',
    'walker'     => new Bootstrap_Page_Menu(),
    'title_li'   => null,
    'child_of'   => 13,
    'depth'      => 2,
) )?>
                </ul>
                <!--
<ul class="list-inline">
<li><a href="">caca</a></li>
<li><a href="">caca</a></li>
<li><a href="">caca</a></li>

</ul>
-->
            </div>
        </div>

    </div>


</article><!-- #post-## -->
