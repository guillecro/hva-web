<article id="post-<?php the_ID(); ?>" <?php post_class('remate'); ?>>
    <div class="col-md-6">
        <a href="<?php echo wp_get_attachment_url( get_post_thumbnail_id()); ?>">
        <img src="<?php echo wp_get_attachment_url( get_post_thumbnail_id()); ?>" class="img-responsive" alt="">
        </a>
    </div>
    <div class="col-md-6">
        <header class="entry-header">
            <?php the_title( '<h1 class="entry-title text-center">', '</h1>' ); ?>
        </header><!-- .entry-header -->

        <div class="entry-content">
            <?php the_content(); ?>
        </div><!-- .entry-content -->
        <div class="otros-remates">
            <h4 class="entry-title">Otros remates</h4>
            <ul class="list-inline">

                <?php wp_list_pages( array(
    'menu_class' => 'list-inline',
    'walker'     => new Bootstrap_Page_Menu(),
    'title_li'   => null,
    'child_of'   => 74,
    'depth'      => 2,
) )?>
            </ul>
            <!--
<ul class="list-inline">
<li><a href="">caca</a></li>
<li><a href="">caca</a></li>
<li><a href="">caca</a></li>

</ul>
-->
        </div>
    </div>


</article><!-- #post-## -->
