<?php
/**
 * Template part for displaying single posts.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package _s
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class('container container-body'); ?>>
    <div class="row">
        <div class="col-xs-12 col-md-10 col-md-offset-1">

            <div class="">
                <?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>
            </div>
<!--
            <div class="entry-meta">
                <?php
                _s_posted_on();
                ?>
            </div>
-->
            <!-- .entry-meta -->
            <div class="entry-content">
                <?php the_content(); ?>
                <?php
//                wp_link_pages( array(
//                    'before' => '<div class="page-links">' . esc_html__( 'Pages:', '_s' ),
//                    'after'  => '</div>',
//                ) );
                ?>
            </div><!-- .entry-content -->

<!--
            <footer class="entry-footer">
                <?php
                _s_entry_footer();
                ?>
            </footer>
-->
<!--             .entry-footer -->
        </div>
    </div>
</article>

