<?php
/**
 * Bloque de Header
 */

?>


<header class="header">
    <div class="container">
        <div class="row logo-row">
            <div class="col-xs-12">
                <img id="logo" src="<?php echo get_template_directory_uri(); ?>/images/utils/logo.png" class="img-responsive">
            </div>
        </div>
    </div>
    <nav class="navbar navbar-default" role="navigation">

        <div class="container">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <i class="fa fa-bars fa-lg fa-fw"></i> Menú
                </button>
                <!-- navbar-brand is hidden on larger screens, but visible when the menu is collapsed -->
                <!--                            <a class="navbar-brand" href="index.html">Business Casual</a>-->
            </div>
            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav center">
                    <li>
                        <a href="<?php echo get_home_url() ?>"><i class="fa fa-home fa-fw fa-lg"></i></a>
                    </li>
                    <li>
                        <a href="<?php echo get_permalink( get_page_by_path( 'familia' ) )?>">Familia ganadera</a>
                    </li>
                    <li>
                        <a href="<?php echo get_permalink( get_page_by_path( 'empresa' ) )?>">La Empresa</a>
                    </li>
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Ventas <span class="caret"></span></a>
                        <ul class="dropdown-menu">
                            <li><a href="<?php echo get_permalink( get_page_by_path( 'ventas-particulares' ) )?>">Particulares</a></li>
                            <li class="divider"></li>
                            <li class="titulo">Remates</li>
                            <?php
    wp_list_pages( array(
        'menu_class' => 'nav navbar-nav',
        'walker'     => new Bootstrap_Page_Menu(),
        'title_li'   => null,
        'child_of'   => 13,
        'depth'      => 2,
    ) )
                            ?>
                        </ul>
                    </li>
                    <li>
                        <a href="<?php echo get_permalink( get_page_by_path( 'establecimientos' ) )?>">Establecimientos</a>
                    </li>


                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Contacto <span class="caret"></span></a>
                        <ul class="dropdown-menu">
                            <li><a href="<?php echo get_permalink( get_page_by_path( 'contacto' ) )?>">Información</a></li>
                            <li><a href="<?php echo get_permalink( get_page_by_path( 'reclamos' ) )?>">Reclamos</a></li>
                        </ul>
                    </li>
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container -->
    </nav>
</header>
