<?php
/**
 * The template for displaying 404 pages (not found).
 *
 * @link https://codex.wordpress.org/Creating_an_Error_404_Page
 *
 * @package _s
 */

get_header(); ?>

<div id="primary" class="content-area">
    <main id="main" class="site-main" role="main">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <section class="error-404 not-found container container-body">
                        <header class="page-header text-center">
                           <img src="<?php echo get_template_directory_uri(); ?>/images/utils/404.png" alt="" class="img-responsive" style="margin: 10px auto;">
                            <h1 class="page-title" style="font-weight: 300">La pagina no existe o no puede ser encontrada</h1>
                        </header><!-- .page-header -->

                        <div class="page-content text-center">
                            <h4><a href="#"><i class="fa fa-home fa-fw"></i>Volver al inicio</a></h4>

                        </div><!-- .page-content -->
                    </section><!-- .error-404 -->
                </div>
            </div>
        </div>

    </main><!-- #main -->
</div><!-- #primary -->

<?php get_footer(); ?>
