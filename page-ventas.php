<?php
/**
 * The main template file.
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package _s
 */

get_header(); ?>

<div id="primary" class="content-area">
    <main id="main" class="site-main" role="main">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <section class="error-404 not-found container container-body">
                        <header class="page-header text-center">
                            <img src="<?php echo get_template_directory_uri(); ?>/images/utils/404.png" alt="" class="img-responsive" style="margin: 10px auto;">
                            <h1 class="page-title" style="font-weight: 300">La pagina no existe o no puede ser encontrada</h1>
                        </header><!-- .page-header -->

                        <div class="page-content text-center">
                            <h4><a href="#"><i class="fa fa-home fa-fw"></i>Volver al inicio</a></h4>

                        </div><!-- .page-content -->
                    </section><!-- .error-404 -->
                </div>
            </div>
        </div>

    </main><!-- #main -->
</div><!-- #primary -->
<!--S
<div id="ventas" class="container">
<div class="row">
<div class="col-xs-12 col-md-6">
<div class="bloque-venta">
<h1 class="text-center">V<span class="lowcase">entas particulares</span></h1>
<a href="<?php echo get_permalink( get_page_by_path( 'ventas-particulares' ) )?>">
<div class="img-container">
<img src="<?php echo  wp_get_attachment_url(88)?>" alt="" class="cropped">
</div>
</a>
<div class="texto-intro text-justify">
Benjamín Tomkins, con ganado de su propiedad. Fue él uno de los primeros en dedicarse a mejorar animales. os bovinos de carne Hereford tuvieron su origen a comienzos del siglo XVIII en el condado de Herefordshire, Inglaterra, como consecuencia de trabajos realizados por Benjamín Tomkins, con ganado de su propiedad. Fue él uno de los primeros en dedicarse a mejorar animales os bovinos de carne Hereford tuvieron su origen a comienzos del siglo XVIII en el condado de Herefordshire, Inglaterra, como consecuencia de trabajos realizados por Benjamín Tomkins,
</div>
<h4 class="contactanos text-right">
<a href="<?php echo get_permalink( get_page_by_path( 'ventas-particulares' ) )?>">Contactanos<i class="fa fa-angle-double-right fa-fw"></i></a>
</h4>
</div>
</div>
<div class="col-xs-12 col-md-6">
<?php
    $mypages = get_pages( array(
        'child_of' => $post->ID,
        'sort_column' => 'post_date',
        'sort_order' => 'menu_order'
    ));

foreach( $mypages as $page ) {
    $content = $page->post_content;
    if ( ! $content ) // Check for empty page
        continue;
    $content = apply_filters( 'the_content', $content );
?>
<div class="bloque-venta">
<?php if ( has_post_thumbnail($page->ID) ): ?>
<div class="img-container remate">
<img src="<?php echo wp_get_attachment_url( get_post_thumbnail_id($page->ID) ); ?>" alt="" class="cropped">
</div>
<?php endif ?>
<h2 class="titulo-remate">
<a href="<?php echo get_page_link( $page->ID ); ?>">
<?php echo $page->post_title; ?><i class="fa fa-angle-double-right fa-fw"></i>
</a>
</h2>
</div>
<?php
}
?>
</div>
</div>
</div>
-->
<?php get_footer(); ?>
