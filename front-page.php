<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package _s
 */


if ( is_home() ) :
get_header( 'home' );
else :
get_header();
endif;
?>

<!--           CONTENIDO A PARTIR DE ACA-->
<div id="fila2">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-md-6 foto-familiar">
                <div class="img-container">

                    <img src="<?php echo get_template_directory_uri(); ?>/images/fotos/Flia.%20Ea.%20San%20Juan.jpg" class="cropped" alt="">
                </div>
            </div>
            <div class="col-xs-12 col-md-6">
                <div class="bloque descripcion">
                    <h3>F<span class="lowcase">AMILIA DE GANADEROS</span></h3>
                    <p>
                        <?php
                        echo gcb('inicio-intro-familia');
                        ?>
                    </p>
                    <a href="<?php echo get_permalink( get_page_by_path( 'familia' ) )?>">Leer mas <i class="fa fa-caret-down fa-fw"></i></a>
                </div>
            </div>
        </div>
    </div>
</div>
<div id="fila3">
    <div class="container">
        <div class="row">
            <?php
            $args = array( 'numberposts' => 3, 'category_name' => 'acerca' );
            $lastposts = get_posts( $args );
            $post = $lastposts[0];
            setup_postdata( $GLOBALS['post'] =& $post );
            ?>
            <a href="<?php echo get_permalink(); ?>">
                <div class="col-sm-12 col-md-5">
                    <div class="bloque">
                        <h5><?php the_title() ?></h5>
                        <?php if ( has_post_thumbnail() ): ?>
                        <div class="img-container">
                            <?php $url = wp_get_attachment_url( get_post_thumbnail_id($post->ID) ); ?>
                            <img src="<?php echo $url ?>" class="cropped" alt="">
                        </div>
                        <?php endif ?>
                        <?php the_excerpt(); ?>
                    </div>
                </div>
            </a>
            <?php
            $post = $lastposts[1];
            setup_postdata( $GLOBALS['post'] =& $post );
            ?>
            <a href="<?php echo get_permalink(); ?>">
                <div class="col-sm-6 col-md-4">
                    <div class="bloque">
                        <h5><?php the_title(); ?></h5>
                        <?php if ( has_post_thumbnail() ): ?>
                        <div class="img-container">
                            <?php $url = wp_get_attachment_url( get_post_thumbnail_id($post->ID) ); ?>
                            <img src="<?php echo $url ?>" class="cropped" alt="">
                        </div>
                        <?php endif ?>
                        <?php the_excerpt(); ?>
                    </div>
                </div>
            </a>
            <?php
            $post = $lastposts[2];
            setup_postdata( $GLOBALS['post'] =& $post );
            ?>
            <a href="<?php echo get_permalink(); ?>">
                <div class="col-sm-6 col-md-3">
                    <div class="bloque">
                        <h5><?php the_title(); ?></h5>
                        <?php if ( has_post_thumbnail() ): ?>
                        <div class="img-container">
                            <?php $url = wp_get_attachment_url( get_post_thumbnail_id($post->ID) ); ?>
                            <img src="<?php echo $url ?>" class="cropped" alt="">
                        </div>
                        <?php endif ?>
                        <?php the_excerpt(); ?>
                    </div>
                </div>
            </a>
        </div>
    </div>
</div>

<?php get_footer(); ?>
